'use strict';

const flights =
    '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';
const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];



const openingHours = {
    //estoy desestructurando el array y le pongo el nombre que quiero que tenga
    [weekdays[3]]: {
        open: 12,
        close: 22,
    },
    fri: {
        open: 11,
        close: 23,
    },
    sat: {
        open: 0, // Open 24 hours
        close: 24,
    },
};

const restaurant = {
    name: 'Classico Italiano',
    location: 'Via Angelo Tavanti 23, Firenze, Italy',
    categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
    starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
    mainMenu: ['Pizza', 'Pasta', 'Risotto'],
    order: function (starterIndex, mainIndex) {
        return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]]; //le pongo los corchetes para que me devuelva un array y le envio los indices de los arrays
    },
    orderPasta: function (ing1, ing2, ing3) {
        console.log(`Here is your delicious pasta with ${ing1}, ${ing2} and ${ing3}.`);
    },
    orderPizza: function (mainIngredient, ...otherIngredients) {//al poner los ... se estan guardando los valores en un array
        console.log(mainIngredient);
        console.log(otherIngredients);
        console.log([mainIngredient, ...otherIngredients]);//operador spread para agregar los valores a un array
    },
};


/* 1
//destructuring arrays
const arr = [2, 3, 4];
const [a, b, c] = arr;//estoy asignando los valores del array a las variables a, b, c
console.log(a, b, c);

const [first, , second] = restaurant.categories;//estoy asignando los valores del array a las variables first, second y estoy saltando el segundo valor
console.log(first, second);//Italian Vegetarian

//resivo los valores de la funcion order ya que me devuelve un array
const [starter, mainCourse] = restaurant.order(2, 0);//estoy asignando los valores del array a las variables starter, mainCourse
console.log(mainCourse, starter);//Garlic Bread Pizza, pero no me lo trae como array

const nested = [2, 4, [5, 6]];
const [i, , [j, k]] = nested;
//estoy asignando los valores del array a las variables i, j, k, estoy desestructurando un array dentro de otro array le pongo los corchetes para el desestructurado
console.log(i, j, k);//2 5 6


//default values
const [p = 1, q = 1, r = 1] = [8, 9];
//estoy asignando los valores del array a las variables p, q, r y si no hay valor le asigno un valor por defecto, el = es para asignar un valor por defecto
 */

/* 2
//spread operator
const arr = [7, 8, 9];
const badNewArr = [1, 2, arr[0], arr[1], arr[2]]; //la forma antigua de hacerlo para agregar los valores de un array a otro array

const newArr = [1,2, ...arr];//la forma nueva de hacerlo para agregar los valores de un array a otro array, le pongo ... y el nombre del array

const newMenu = [...restaurant.mainMenu, 'Gnocci'];//agrego un nuevo valor al array mainMenu

//copy array
const mainMenuCopy = [...restaurant.mainMenu];//copio el array mainMenu
//join 2 arrays
const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];//uno 2 arrays en uno solo

//  Iterables: arrays, strings, maps, sets. NOT objects
const str = 'Matias';
const letters = [...str, ' ', 'S.'];//convierto un string en un array y le agrego mas valores
console.log(...str);// M a t i a s, pero no me lo trae como array

//real world example
const ingredients = [1,2,3];

restaurant.orderPasta(...ingredients);// le envio los valores del array a la funcion orderPasta, pero los valores no vienen en array, vienen separados por coma


//objects
const newRestaurant = {foundedIn: 1998, ...restaurant, founder: 'Guiseppe'};//agrego un nuevo valor al objeto restaurant, compio todos los valores y los agregos al nuevo objeto
*/


/* 3
//Rest Operator

//1) Destructuring
//Spread, because on RIGHT side of =
 Operador Spread: Se utiliza para descomponer elementos iterables (como arrays, objetos o strings)
en elementos individuales. Se utiliza en el lado derecho de una asignación.
Por ejemplo, se puede utilizar para copiar arrays, fusionar arrays u objetos,
pasar elementos de un array a una función como argumentos separados.
const arr = [1, 2, ...[3, 4]];

//Rest, because on LEFT side of =, tambien d
Operador Rest: Se utiliza para recoger los
 elementos restantes en un array o propiedades
  en un objeto en una nueva matriz u objeto.
  Se utiliza en el lado izquierdo de una asignación. Por ejemplo, se puede utilizar en la definición de una función
  para recoger todos los argumentos en un array.
const [a, b, ...others] = [1, 2, 3, 4, 5];//estoy asignando los valores del array a las variables a, b y el resto de los valores los asigno a la variable others


 const [...rest] = [...spread]


const [pizza, , risotto, ...otherFood] = [ //operador rest
    ...restaurant.mainMenu,//operador spread
    ...restaurant.starterMenu,//operador spread
];

//objects
const { sat, ...weekdays } = restaurant.openingHours;//operador rest, debe ser el mismo nombre que el objeto



//2) Functions
const add = function (...numbers){//al poner los ... se estan guardando los valores en un array
    let sum = 0;
    for(let i = 0; i < numbers.length; i++){
        sum += numbers[i];
    }
}

add(2,3);
add(5,3,7,2);
add(8,2,5,3,2,1,4);

restaurant.orderPizza('mushrooms', 'onion', 'olives', 'spinach');//le envio los parameetros a la funcion

*/


/* 4

console.log("----------------------OR---------------------------");

// Use ANY data type, return ANY data type, short-circuiting
console.log(3 || 'Matias');//si el primer valor es verdadero el siguiente valor no se evalua
console.log('' || 'Matias');//si el primer valor es falso el siguiente valor se evalua
console.log(true || 0);//si el primer valor es verdadero el siguiente valor no se evalua
console.log(undefined || null);//si el primer valor es falso el siguiente valor se evalua

console.log(undefined || 0 || '' || 'Hello' || 23 || null);//si hay un valor verdadero se detiene la evaluacion y devuelve ese valor


restaurant.numGuests = 23;
const guest1 = restaurant.numGuests ? //si hay valor verdadero devuelve el primer valor
    restaurant.numGuests : 10; // si es falso devuelve el segundo valor
console.log(guest1);

const guest2 = restaurant.numGuests || 10;//si hay valor verdadero devuelve el primer valor, si es falso devuelve el segundo valor
console.log(guest2);


console.log("----------------------AND---------------------------");

console.log(0 && 'Matias');//si el primer valor es verdadero el siguiente valor se evalua
console.log(7 && 'Matias');//si el primer valor es falso el siguiente valor se evalua

console.log('Hello' && 23 && null && 'Matias');//si hay un valor falso se detiene la evaluacion y todo el resultado es falso

//practical example
if (restaurant.orderPizza) {
    restaurant.orderPizza('mushrooms', 'spinach');
}
//el objeto restaurant.orderPizza existe y es verdadero y el otro tambien es verdadero
restaurant.orderPizza && restaurant.orderPizza('mushrooms', 'spinach');//si hay valor falsos se detiene la evaluacion y todo el resultado es falso

*/

/* 5
// The Nullish Coalescing Operator (??)
restaurant.guestNum = 0;
const guest = restaurant.guestNum || 10;//si hay valor verdadero devuelve el primer valor, si es falso devuelve el segundo valor se un valor verdadero
console.log(guest);

//Nullish: null and undefined (NOT 0 or ''),es decir si el valor es null o undefined se toma el valor por defecto despues del operador ??
const guestCorrect = restaurant.guestNum ?? 10;
console.log(guestCorrect);

*/

/* 6
//Logial Operators
const rest1 = {
    name: 'Capri',
    numGuest: 20,
}

const rest2 = {
    name:'la parrilla',
    owner: 'Matias',
    numGuest: 0,
}

//rest1.numGuest = rest1.numGuest || 10;
//rest2.numGuest = rest2.numGuest || 10;
//OR ASSIGNMENT OPERATOR
//los valores falsy son en el operador OR -> 0, '', undefined, null, NaN.
//rest1.numGuest ||= 10; //forma corta, evaluea si rest1.numGuest es flaso y si es falso le asigna el valor 10 o si es verdadero no hace nada
//rest2.numGuest ||= 10; //

//Nullish Coalescing Operator
//los valores falsy son en el operador nullish -> undefined, null
rest1.numGuest ??= 10; //
rest2.numGuest ??= 10; //


rest1.owner = rest1.owner && 'Matias'; //evalua los 2 valores y retona cuando hace cortecircuito
//rest2.owner = rest2.owner && 'Matias'; //como el valor es verdadero me devuelve el ultimo valor cuando hace el cortecircuito

//rest1.owner &&= 'Matias'; //solo se asgina matias si rest1.owner es verdadero
rest2.owner &&= 'Matias';
console.log(rest1)
console.log(rest2)
*/

/* 7
//Looping Arrays: The for-of Loop
const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];

//esto es para recorrer un array con un for of, primero le pongo un nombre y despues el nombre del array
for (const item of menu) console.log(item);

for (const [i,el] of menu.entries()) { //al poner entries() me devuelve el indice y el valor, si no lo pongo me devuelve solo el valor
    console.log(`${i + 1}: ${el}`);
}
*/
