'use strict';

/*
//1) Default parameters
const bookings = [];

const createBooking = function (
    flightNum,
    numPassengers = 1, // default value
    price = 199 * numPassengers // default value si no viene el parametro
) {
    const booking = {
        flightNum,
        numPassengers,
        price
    };
    console.log(booking);
    bookings.push(booking);
}

createBooking('LH123');
createBooking('LH123', 2, 800);
createBooking('LH123', 5);
*/

/*
2) How passing arguments works: value vs reference
const flight = 'LH234'
const matias = {
    name: 'Matias Godoy',
    passport: 21451265
}


const checkIn = function (flightNum, passenger) {
    passenger.name = 'Mr. ' + passenger.name;

    if (passenger.passport === 21451265) {
        console.log('Checked in')
    } else {
        console.log('Wrong passport!')
    }
}

const newPassport = function (person) {
    person.passport = Math.trunc(Math.random() * 1000000000)
}

newPassport(matias) // se cambia el objeto original
console.log(matias)
checkIn(flight, matias)

 */

/*
//3) Callback functions
const oneWord = function (str) {
    return str.replace(/ /g, '').toLowerCase();
}

const upperFirstWord = function (str) {
    const [first, ...others] = str.split(' ');
    return [first.toUpperCase(), ...others].join(' ');
}

// Higher-order function
const transformer = function (str, fn) {
    console.log(`Original string: ${str}`);
    console.log(`Transformed string: ${fn(str)}`);
    console.log(`Transformed by: ${fn.name}`); //el fn.name es el nombre de la funcion
}

transformer('JavaScript is the best!', upperFirstWord);
console.log('-------------------');
transformer('JavaScript is the best!', oneWord);
*/


/*
//4) Functions returning functions, closures
const greet = function (greeting) {

    return function (name) {
        console.log(`${greeting} ${name}`);
    }
}

const greeterHey = greet('Hey');//el gteeterHey es una funcion que recibe un parametro de la primera funcion
greeterHey('Matias'); //le paso el parametro a la segunda funcion
greeterHey('Steven');


//otra forma de hacer lo mismo pero con arrow functions
const greetArrow = greeting => name => console.log(`${greeting} ${name}`);

greetArrow('Hola')('Matias');//le paso los 2 parametros a la funciones

 */